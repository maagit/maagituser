<?php
	return [
		'extensions-maagituser' => [
			'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
			'source' => 'EXT:maagituser/Resources/Public/Icons/Extension.png',
		],
		'extensions-maagituser_login' => [
			'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
			'source' => 'EXT:maagituser/Resources/Public/Icons/login.png',
		],
		'extensions-maagituser_registration' => [
			'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
			'source' => 'EXT:maagituser/Resources/Public/Icons/registration.png',
		],
		'extensions-maagituser_recovery' => [
			'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
			'source' => 'EXT:maagituser/Resources/Public/Icons/recovery.png',
		],
		'extensions-maagituser_mutation' => [
			'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
			'source' => 'EXT:maagituser/Resources/Public/Icons/mutation.png',
		],
		'extensions-maagituser_delete' => [
			'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
			'source' => 'EXT:maagituser/Resources/Public/Icons/delete.png',
		]
	];
?>