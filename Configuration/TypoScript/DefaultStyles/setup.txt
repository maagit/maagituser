# Flag to check, if this template is loaded
plugin.tx_maagituser {
	settings {
		additionalCSSLoaded = 1
	}
}

# Include Maagituser javascripts
page {
	includeJS {
		tx_maagituser_556_1 = EXT:maagituser/Resources/Public/JavaScript/Login/LoginMain.js
	}
}

# Include Maagituser default styles
page {
	headerData {
		556 = TEXT
		556 {
			value (
				<style>
					.tx-maagituser_header {
						display: block;
					}
					.tx-maagituser_title {
						display: block;
					}
					.tx-maagituser_content .row {
						margin-right: 0;
					}
					.tx-maagituser::before {
						content: '';
						position: fixed;
						z-index: 1;
						height: 100%;
						width: 100%;
						top: 0;
						left: 0;
						background: rgba(0,0,0,.5);
						opacity: 0;
						visibility: hidden;
						transition: opacity .4s,visibility .4s;
					}
					.tx-maagituser_open::before {
						opacity: 1;
						visibility: visible;
					}
					.tx-maagituser_login .tx-maagituser_content,
					.tx-maagituser_logout .tx-maagituser_content {
						position: absolute;
						background-color: white;
						width: 0;
						height: 0;
						padding: 0;
						right: 0;
						top: 28px;
						z-index: 2;
						overflow: hidden;
						border-radius: 0.25em;
						transition: height .4s .1s, width .4s .1s, box-shadow .3s, padding .4s .1s;
						transition-timing-function: ease, ease, ease, ease;
						transition-timing-function: cubic-bezier(0.67, 0.17, 0.32, 0.95);
						box-shadow: 0 4px 30px rgba(0,0,0,0.17);
						pointer-events: auto;
						-ms-flex-direction: column;
						flex-direction: column;
					}
					.tx-maagituser_open .tx-maagituser_login .tx-maagituser_content,
					.tx-maagituser_open .tx-maagituser_logout .tx-maagituser_content {
						visibility: visible;
						padding: 15px;
					}
					.tx-maagituser_open .tx-maagituser_login .tx-maagituser_content {
						width: 520px;
						height: 217px;
					}
					.tx-maagituser_open .tx-maagituser_permalogin .tx-maagituser_content {
						height: 264px;
					}
					.tx-maagituser_open .tx-maagituser_recovery .tx-maagituser_content {
						height: 270px;
					}
					.tx-maagituser_open .tx-maagituser_permalogin.tx-maagituser_recovery .tx-maagituser_content {
						height: 318px;
					}
					.tx-maagituser_open .tx-maagituser_error .tx-maagituser_content {
						height: 276px;
					}
					.tx-maagituser_open .tx-maagituser_error.tx-maagituser_permalogin .tx-maagituser_content {
						height: 326px;
					}
					.tx-maagituser_open .tx-maagituser_error.tx-maagituser_recovery .tx-maagituser_content {
						height: 326px;
					}
					.tx-maagituser_open .tx-maagituser_error.tx-maagituser_permalogin.tx-maagituser_recovery .tx-maagituser_content {
						height: 374px;
					}
					.tx-maagituser_open .tx-maagituser_logout .tx-maagituser_content {
						width: 520px;
						height: 167px;
					}
					.tx-maagituser_open .tx-maagituser_logout.tx-maagituser_mutation .tx-maagituser_content {
						height: 218px;
					}
					.tx-maagituser_open .tx-maagituser_logout.tx-maagituser_delete .tx-maagituser_content {
						height: 218px;
					}
					.tx-maagituser_open .tx-maagituser_logout.tx-maagituser_error .tx-maagituser_content {
						height: 208px;
					}
					.tx-maagituser_open .tx-maagituser_logout.tx-maagituser_error.tx-maagituser_mutation .tx-maagituser_content {
						height: 253px;
					}
					.tx-maagituser_open .tx-maagituser_logout.tx-maagituser_error.tx-maagituser_delete .tx-maagituser_content {
						height: 253px;
					}
					@media (max-width: 480px) {
						.tx-maagituser_login .tx-maagituser_content,
						.tx-maagituser_logout .tx-maagituser_content {
							position: fixed				!important;
							width: 380px				!important;
							margin-top:	52px			!important;
						}
						.tx-maagituser_login .tx-maagituser_content label,
						.tx-maagituser_logout .tx-maagituser_content label {
							display: none;
						}
						.tx-maagituser_login .form-check-label {
							display: block				!important;
						}
						.tx-maagituser_content .row {
							margin-left: 0;
						}
						.tx-maagituser_open .tx-maagituser_login .tx-maagituser_content {
							width: 380px;
							height: 217px;
						}
						.tx-maagituser_open .tx-maagituser_permalogin .tx-maagituser_content {
							height: 282px;
						}
						.tx-maagituser_open .tx-maagituser_recovery .tx-maagituser_content {
							height: 270px;
						}
						.tx-maagituser_open .tx-maagituser_permalogin.tx-maagituser_recovery .tx-maagituser_content {
							height: 338px;
						}
						.tx-maagituser_open .tx-maagituser_error .tx-maagituser_content {
							height: 293px;
						}
						.tx-maagituser_open .tx-maagituser_error.tx-maagituser_permalogin .tx-maagituser_content {
							height: 360px;
						}
						.tx-maagituser_open .tx-maagituser_error.tx-maagituser_recovery .tx-maagituser_content {
							height: 350px;
						}
						.tx-maagituser_open .tx-maagituser_error.tx-maagituser_permalogin.tx-maagituser_recovery .tx-maagituser_content {
							height: 415px;
						}
						.tx-maagituser_open .tx-maagituser_logout .tx-maagituser_content {
							width: 380px;
							height: 167px;
						}
						.tx-maagituser_open .tx-maagituser_logout.tx-maagituser_mutation .tx-maagituser_content {
							height: 218px;
						}
						.tx-maagituser_open .tx-maagituser_logout.tx-maagituser_delete .tx-maagituser_content {
							height: 218px;
						}
						.tx-maagituser_open .tx-maagituser_logout.tx-maagituser_error .tx-maagituser_content {
							height: 205px;
						}
						.tx-maagituser_open .tx-maagituser_logout.tx-maagituser_error.tx-maagituser_mutation .tx-maagituser_content {
							height: 253px;
						}
						.tx-maagituser_open .tx-maagituser_logout.tx-maagituser_error.tx-maagituser_delete .tx-maagituser_content {
							height: 253px;
						}
					}
				</style>
			)
		}
	}
}