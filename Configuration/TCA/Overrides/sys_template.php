<?php
call_user_func(function () {
	// add static template
	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('maagituser', 'Configuration/TypoScript', 'Maagituser');
	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('maagituser', 'Configuration/TypoScript/DefaultStyles', 'Maagituser login on every page CSS (optional)');
});
?>