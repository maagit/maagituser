<?php
defined('TYPO3') || die('Access denied.');

// ---------------------------------------------------------------------------------------------------------------

// register plugin
$pluginSignature = \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Maagituser',																			// extension name
    'Login',																				// plugin name
    'LLL:EXT:maagituser/Resources/Private/Language/login.xlf:plugins.title',				// plugin title
    'extensions-maagituser_login',															// icon identifier
    'maagituser', 	 																		// group
    'LLL:EXT:maagituser/Resources/Private/Language/login.xlf:plugins.description'			// plugin description
);

// add flexform configuration field
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
	'tt_content',
	'--div--;Configuration,pi_flexform,',
	$pluginSignature,
	'after:subheader'
);

// add flexform definition file
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
	'*',
	'FILE:EXT:maagituser/Configuration/FlexForms/Login.xml',
	$pluginSignature
);

// ---------------------------------------------------------------------------------------------------------------

// register plugin
$pluginSignature = \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Maagituser',																			// extension name
    'Recovery',																				// plugin name
    'LLL:EXT:maagituser/Resources/Private/Language/recovery.xlf:plugins.title',				// plugin title
    'extensions-maagituser_recovery',														// icon identifier
    'maagituser', 	 																		// group
    'LLL:EXT:maagituser/Resources/Private/Language/recovery.xlf:plugins.description'		// plugin description
);

// add flexform configuration field
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
	'tt_content',
	'--div--;Configuration,pi_flexform,',
	$pluginSignature,
	'after:subheader'
);

// add flexform definition file
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
	'*',
	'FILE:EXT:maagituser/Configuration/FlexForms/Recovery.xml',
	$pluginSignature
);

// ---------------------------------------------------------------------------------------------------------------

// register plugin
$pluginSignature = \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Maagituser',																			// extension name
    'Registration',																			// plugin name
    'LLL:EXT:maagituser/Resources/Private/Language/registration.xlf:plugins.title',			// plugin title
    'extensions-maagituser_registration',													// icon identifier
    'maagituser', 	 																		// group
    'LLL:EXT:maagituser/Resources/Private/Language/registration.xlf:plugins.description'	// plugin description
);

// add flexform configuration field
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
	'tt_content',
	'--div--;Configuration,pi_flexform,',
	$pluginSignature,
	'after:subheader'
);

// add flexform definition file
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
	'*',
	'FILE:EXT:maagituser/Configuration/FlexForms/Registration.xml',
	$pluginSignature
);

// ---------------------------------------------------------------------------------------------------------------

// register plugin
$pluginSignature = \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Maagituser',																			// extension name
    'Mutation',																				// plugin name
    'LLL:EXT:maagituser/Resources/Private/Language/mutation.xlf:plugins.title',				// plugin title
    'extensions-maagituser_mutation',														// icon identifier
    'maagituser', 	 																		// group
    'LLL:EXT:maagituser/Resources/Private/Language/mutation.xlf:plugins.description'		// plugin description
);

// add flexform configuration field
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
	'tt_content',
	'--div--;Configuration,pi_flexform,',
	$pluginSignature,
	'after:subheader'
);

// add flexform definition file
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
	'*',
	'FILE:EXT:maagituser/Configuration/FlexForms/Mutation.xml',
	$pluginSignature
);

// ---------------------------------------------------------------------------------------------------------------

// register plugin
$pluginSignature = \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Maagituser',																			// extension name
    'Delete',																				// plugin name
    'LLL:EXT:maagituser/Resources/Private/Language/delete.xlf:plugins.title',				// plugin title
    'extensions-maagituser_delete',															// icon identifier
    'maagituser', 	 																		// group
    'LLL:EXT:maagituser/Resources/Private/Language/delete.xlf:plugins.description'			// plugin description
);

// add flexform configuration field
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
	'tt_content',
	'--div--;Configuration,pi_flexform,',
	$pluginSignature,
	'after:subheader'
);

// add flexform definition file
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
	'*',
	'FILE:EXT:maagituser/Configuration/FlexForms/Delete.xml',
	$pluginSignature
);

// ---------------------------------------------------------------------------------------------------------------

?>