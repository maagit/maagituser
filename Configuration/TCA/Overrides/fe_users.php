<?php
call_user_func(function () {
    // Define additional columns
	$additionalColumns = [
		'maagituser_verifyHash' => [
			'exclude' => true,
			'label' => 'LLL:EXT:maagituser/Resources/Private/Language/Database.xlf:maagituser_verifyHash',
            'config' => [
                'type' => 'input',
				'size' => 50,
                'eval' => 'trim'
            ]
		]
	];

	// Add new columns to fe_users
	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns(
	        'fe_users',
	        $additionalColumns
	);
	
	// Add new column "maagituser_verifyHash" to fe_users backend form
	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
		'fe_users',
		'maagituser_verifyHash',
		'',
		'after:tx_extbase_type'
	);
});
?>