<?php
declare(strict_types = 1);

return [
	// fe_users property mapping
	\Maagit\Maagituser\Domain\Model\User::class => [
        'tableName' => 'fe_users',
	],

	// file reference mapping
	Maagit\Maagituser\Domain\Model\FileReference::class => [
		'tableName' => 'sys_file_reference',
		'properties' => [
			'originalFileIdentifier' => [
				'fieldName' => 'uid_local'
			]
		]
	]
];