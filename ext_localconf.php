<?php
defined('TYPO3') || die('Access denied.');

// configure plugin
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Maagituser',
	'Login',
	[
		\Maagit\Maagituser\Controller\LoginController::class => 'show, login, logout'
	],
	[
		\Maagit\Maagituser\Controller\LoginController::class => 'show, login, logout'
	],
	\TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);

// configure plugin
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Maagituser',
	'Recovery',
	[
		\Maagit\Maagituser\Controller\RecoveryController::class => 'show, send, showChange, change'
	],
	[
		\Maagit\Maagituser\Controller\RecoveryController::class => 'show, send, showChange, change'
	],
	\TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);

// configure plugin
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Maagituser',
	'Registration',
	[
		\Maagit\Maagituser\Controller\RegistrationController::class => 'show, register, activate'
	],
	[
		\Maagit\Maagituser\Controller\RegistrationController::class => 'show, register, activate'
	],
	\TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);

// configure plugin
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Maagituser',
	'Mutation',
	[
		\Maagit\Maagituser\Controller\MutationController::class => 'show, save, confirm, activate'
	],
	[
		\Maagit\Maagituser\Controller\MutationController::class => 'show, save, confirm, activate'
	],
	\TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);

// configure plugin
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Maagituser',
	'Delete',
	[
		\Maagit\Maagituser\Controller\DeleteController::class => 'show, delete, confirm, deleteConfirmed'
	],
	[
		\Maagit\Maagituser\Controller\DeleteController::class => 'show, delete, confirm, deleteConfirmed'
	],
	\TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);
