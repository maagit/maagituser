<?php
namespace Maagit\Maagituser\Service;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2022-2022 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagituser
	Package:			Service
	class:				CryptoService

	description:		Crypto and hash code methods.

	created:			2022-02-06
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2022-02-06	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class CryptoService extends \Maagit\Maagituser\Service\BaseService
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
     * @var string
     */
    protected $verifyHash;

    /**
     * @var int
     */
    protected $timestamp;


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */
	/**
     * Get the verifyHash
     *
     * @param string $verifyHash
     */
    public function getVerifyHash()
    {
        return $this->verifyHash;
    }

	/**
     * Get the timestamp
     *
     * @param int $timestamp
     */
	public function getTimestamp($validLinkTime)
    {
		if ($this->timestamp === null)
		{
			$lifetimeInHours = (!empty($validLinkTime)) ? $validLinkTime : 12;
            $this->timestamp = time()+(3600*$lifetimeInHours);
        }
        return $this->timestamp;
    }


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * Send the activation/confirmation link
     *
     * @param	string					$email				the email address
	 * @param	string					$type				the type of activation/confirmation:
	 *														recovery | registration | mutation
	 * @return	string										generated hash
     */
	public function sendLink(string $email, string $type)
	{
		$this->verifyHash = $this->getTimestamp((isset($this->settings[$type.'LinkValidTime'])?$this->settings[$type.'LinkValidTime']:null)).'|'.$this->generateHash();
		$renderService = $this->makeInstance('Maagit\Maagituser\Service\RenderService');
		$body = $renderService->renderTemplate(ucfirst($type).'/EmailLink', array(
			'link' => $this->getVerificationLink($type),
			'valid' => date(\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate($type.'.mail.valid.format', 'maagituser'), $this->getTimestamp((isset($this->settings[$type.'LinkValidTime']))?$this->settings[$type.'LinkValidTime']:null)),
			'sender' => $this->getSenderName($type)
		));

		$emailService = $this->makeInstance('Maagit\Maagituser\Service\EmailService');
		$emailService->sendMail(
			array($this->getSenderAddress($type)),
			array($email),
			\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate($type.'.mail.subject', 'maagituser'),
			$body,
			$body
		);

		return $this->verifyHash;
	}

	/**
     * Check given verification link, if lifetime is valid and hash is in user table
     *
     * @param	variant					$hash			the hash
	 * @return	boolean									is verification link valid?
     */
	public function validateVerificationLink($hash)
	{
        if (!$this->isValidHashLifetime($hash)) {return false;}
		$userRepository = $this->makeInstance('Maagit\Maagituser\Domain\Repository\UserRepository');
		$hashService = $this->makeInstance('TYPO3\CMS\Core\Crypto\HashService');
		if (!$userRepository->findByVerificationHash($hashService->hmac($hash, 'x'))) {return false;}
		return true;
	}


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	/**
     * Generate a hash string
     *
     * @return	string									generated hash
     */
	protected function generateHash()
    {
		$random = $this->makeInstance('TYPO3\CMS\Core\Crypto\Random');
		$hashService = $this->makeInstance('TYPO3\CMS\Core\Crypto\HashService');
        $randomString = $random->generateRandomHexString(16);
        return $hashService->hmac('userSecretHash', $randomString);
    }

	/**
     * Validate given hash
     *
     * @param	variant					$hash				the hash to validate
	 * @return	boolean										is valid?
     */
	protected function isValidHash($hash)
	{
		return (!empty($hash) && is_string($hash) && strpos($hash, '|') === 10);
	}

	/**
     * Validate given hash lifetime
     *
     * @param	variant					$hash				the hash to validate
	 * @return	boolean										is valid lifetime?
     */
	protected function isValidHashLifetime($hash)
	{
	    if ($this->isValidHash($hash))
		{
			$timestamp = (int)\TYPO3\CMS\Core\Utility\GeneralUtility::trimExplode('|', $hash)[0];
		    $currentTimestamp = $this->makeInstance('TYPO3\CMS\Core\Context\Context')->getPropertyFromAspect('date', 'timestamp');
			if ($currentTimestamp <= $timestamp) {return true;}
		}
		return false;
	}

	/**
     * Create the verification link
     *
     * @param	string					$type			the type of activation/confirmation:
	 *													recovery | registration | mutation
	 * @return	string									generated verification link
     */
	protected function getVerificationLink(string $type)
	{
		$uriBuilder = $this->makeInstance('TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder');
		$uriBuilder->setRequest($this->divHelper->getExtbaseRequest());
		$url = $uriBuilder->uriFor($this->getMethod($type), array('hash' => $this->getVerifyHash()), ucfirst($type), 'maagituser', ucfirst($type));
		$host = $_SERVER['HTTP_HOST'];
		$protocol = (isset($_SERVER['HTTPS']) && !empty($_SERVER['HTTPS'])) ? 'https://' : 'http://';
		$url = str_replace('/login?', '/login/'.$type.'?', $url);
		$url = $protocol.$host.$url;
		return $url;
	}


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
	/**
     * Get method, based on given type
     *
     * @param	string					$type			the type of activation/confirmation:
	 *													recovery | registration | mutation
	 * @return	string									the method
     */
	private function getMethod(string $type)
	{
		$method = 'init';
		if ($type == 'recovery') {$method = 'showChange';}
		if ($type == 'registration') {$method = 'activate';}
		if ($type == 'mutation') {$method = 'activate';}
		if ($type == 'delete') {$method = 'deleteConfirmed';}
		return $method;
	}

	/**
     * Get sender address, based on given type
     *
     * @param	string					$type			the type of activation/confirmation:
	 *													recovery | registration | mutation
	 * @return	string									the sender address
     */
	private function getSenderAddress(string $type)
	{
		$address = 'dummy@dummy.com';
		if ($type == 'recovery') {$address = $this->settings[$type.'SenderAddress'];}
		if ($type == 'registration') {$address = $this->settings[$type.'VerifySenderAddress'];}
		if ($type == 'mutation') {$address = $this->settings[$type.'VerifySenderAddress'];}
		if ($type == 'delete') {$address = $this->settings[$type.'VerifySenderAddress'];}
		return $address;
	}

	/**
     * Get sender name, based on given type
     *
     * @param	string					$type			the type of activation/confirmation:
	 *													recovery | registration | mutation
	 * @return	string									the sender name
     */
	private function getSenderName(string $type)
	{
		$name = 'dummy';
		if ($type == 'recovery') {$name = (!empty($this->settings[$type.'SenderName'])) ? $this->settings[$type.'SenderName'] : $_SERVER['HTTP_HOST'];}
		if ($type == 'registration') {$name = (!empty($this->settings[$type.'VerifySenderName'])) ? $this->settings[$type.'VerifySenderName'] : $_SERVER['HTTP_HOST'];}
		if ($type == 'mutation') {$name = (!empty($this->settings[$type.'VerifySenderName'])) ? $this->settings[$type.'VerifySenderName'] : $_SERVER['HTTP_HOST'];}
		if ($type == 'delete') {$name = (!empty($this->settings[$type.'VerifySenderName'])) ? $this->settings[$type.'VerifySenderName'] : $_SERVER['HTTP_HOST'];}
		return $name;
	}
}