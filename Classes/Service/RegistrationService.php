<?php
namespace Maagit\Maagituser\Service;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2022-2022 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagituser
	Package:			Service
	class:				PasswordService

	description:		Registration methods.

	created:			2022-01-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2022-01-29	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class RegistrationService extends \Maagit\Maagituser\Service\BaseService
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var \Maagit\Maagituser\Domain\Repository\UserRepository
     */
    protected $userRepository;

	/**
	 * @var \Maagit\Maagituser\Service\PasswordService
     */
    protected $passwordService;

	/**
	 * @var \Maagit\Maagituser\Helper\DivHelper
     */
    protected $divHelper;


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	/**
     * Constructor, initialize objects
     *
     * @return void
     */
	public function initializeObject()
	{
		// inject repositories and services
		$this->userRepository = $this->makeInstance('Maagit\Maagituser\Domain\Repository\UserRepository');
		$this->passwordService = $this->makeInstance('Maagit\Maagituser\Service\PasswordService');
		$this->divHelper = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('Maagit\Maagituser\Helper\DivHelper');
	}


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * Validate user registration form
     *
     * @param	\Maagit\Maagituser\Domain\Model\User		$user			the registration datas
	 * @return	array														array of errors | empty array
     */
	public function validate(\Maagit\Maagituser\Domain\Model\User &$user)
	{
		$errors = array();
		// validate uniqe username
		$returnValue = $this->validateUniqueUsername(empty($user->getUsername()) ? $user->getEmail() : $user->getUsername());
		if ($returnValue != '') {array_push($errors, $returnValue);}
		// validate required fields
		$returnValue = $this->validateRequiredFields($user);
		if (!empty($returnValue)) {$errors = array_merge($errors, $returnValue);}
		// validate password rules
		$returnValue = $this->validatePasswordRules($user->getPassword());
		if ($returnValue != '') {array_push($errors, $returnValue);}
		// validate password repeat
		if (array_key_exists('passwordRepeat', $user->getRequiredFields()))
		{
			$returnValue = $this->validatePasswordRepeat($user->getPassword(), $user->getPasswordRepeat());
			if ($returnValue != '') {array_push($errors, $returnValue);}
		}
		// validate uploaded image
		if (in_array('image', $user->getFormFields()) && !empty($user->getPicture()['tmp_name']))
		{
			$returnValue = $this->validateImage($user, (!empty($errors)));
			if ($returnValue != '') {array_push($errors, $returnValue);}
		}
		return $errors;
	}


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	/**
     * Validate on required empty fields
     *
     * @param	\Maagit\Maagituser\Domain\Model\User	$user			the registration form object
	 * @return	array													array with given error messages | empty array
     */
	function validateRequiredFields(\Maagit\Maagituser\Domain\Model\User $user)
	{
		$emptyFields = array();
		foreach ($user->getRequiredFields() as $key => $value)
		{
			if (in_array($key, $user->getFormFields()))
			{
				$value = $this->divHelper->getMemberByReflection($user, $key);
				if (empty($value))
				{
					$label = \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('registration.'.$key, 'maagituser');
					array_push($emptyFields, \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('registration.error.required.prefix', 'maagituser').'&laquo;'.$label.'&raquo '.\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('registration.error.required.suffix', 'maagituser'));
				}
			}
		}
		return $emptyFields;
	}

	/**
     * Validate on unique username
     *
     * @param	string								$username			the username
	 * @return	string													error message | empty string
     */
	protected function validateUniqueUsername(string $username)
	{
		if ($this->userRepository->findByEmail($username, true))
		{
			return \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('registration.error.unique', 'maagituser');
		}
		return '';
	}

	/**
     * Validate on correct password rules
     *
     * @param	string								$password			the password
	 * @return	string													error message | empty string
     */
	protected function validatePasswordRules(string $password)
	{
		if (!$this->passwordService->isValidPassword($password))
		{
			return \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('registration.error.password.rules', 'maagituser');
		}
		return '';
	}

	/**
     * Validate on same password and password repeat
     *
     * @param	string								$password			the password
	 * @param	string								$passwordRepeat		the repeated password
	 * @return	string													error message | empty string
     */
	protected function validatePasswordRepeat(string $password, string $passwordRepeat)
	{
		if ($password != $passwordRepeat)
		{
			return \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('registration.error.password.repeat', 'maagituser');
		}
		return '';
	}

	/**
     * Validate and handle uploaded file
     *
     * @param	\Maagit\Maagituser\Domain\Model\User		&$user			the user object
	 * @param	boolean										hasErrors		are there other errors occured?
	 * @return	void
     */
	protected function validateImage(&$user, bool $hasErrors=false)
	{
		if (!in_array(pathinfo($user->getPicture()['name'])['extension'], explode(',', $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext'])))
		{
			return \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('registration.error.file.extension', 'maagituser');
		}
		if (isset($this->settings['registrationMaxUploadSize']) && (int)$this->settings['registrationMaxUploadSize'] > 0)
		{
			if ((int)$user->getPicture()['size'] > (int)$this->settings['registrationMaxUploadSize'])
			{
				return \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('registration.error.file.size', 'maagituser', array($this->settings['registrationMaxUploadSize'], $user->getPicture()['size']));
			}
		}
		if (!$hasErrors)
		{
			$fileService = $this->makeInstance('Maagit\Maagituser\Service\FileService');
			$uploadPath = (isset($this->settings['registrationUploadPath'])) ? $this->settings['registrationUploadPath'] : '';
			$uploadedFileResource = $fileService->saveUploadedFile($user->getPicture(), $uploadPath);
			if ($uploadedFileResource === false)
			{
				return \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('registration.error.file.save', 'maagituser');
			}
			$fileReference = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('Maagit\Maagituser\Domain\Model\FileReference');
			$fileReference->setFile($uploadedFileResource);
			$fileReference->setPid($this->settings['registrationStoragePid']);
			$user->setImage($fileReference);
		}
		return '';
	}


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}