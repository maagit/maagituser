<?php
namespace Maagit\Maagituser\Service;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2022-2022 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagituser
	Package:			Service
	class:				MutationService

	description:		Mutation methods.

	created:			2022-01-31
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2022-01-31	Urs Maag		Initial version
						2022-10-10	Urs Maag		Typo3 12.0.0 compatibility
													- get current user data with new
													  method "$this->getOldUserData"

------------------------------------------------------------------------------------- */


class MutationService extends \Maagit\Maagituser\Service\BaseService
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var \Maagit\Maagituser\Domain\Repository\UserRepository
     */
    protected $userRepository;

	/**
	 * @var \Maagit\Maagituser\Service\PasswordService
     */
    protected $passwordService;

	/**
	 * @var \Maagit\Maagituser\Helper\DivHelper
     */
    protected $divHelper;


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	/**
     * Constructor, initialize objects
     *
     * @return void
     */
	public function initializeObject()
	{
		// inject repositories and services
		$this->userRepository = $this->makeInstance('Maagit\Maagituser\Domain\Repository\UserRepository');
		$this->passwordService = $this->makeInstance('Maagit\Maagituser\Service\PasswordService');
		$this->divHelper = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('Maagit\Maagituser\Helper\DivHelper');
	}


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * Validate user mutation form
     *
     * @param	\Maagit\Maagituser\Domain\Model\User		$user			the registration datas
	 * @return	array														array of errors | empty array
     */
	public function validate(\Maagit\Maagituser\Domain\Model\User &$user)
	{
		$errors = array();
		$currentUserData = $this->userRepository->getOldUserData($user->getUid());
		// validate uniqe username
		$returnValue = $this->validateUniqueUsername($currentUserData['username'], (!in_array('username', $user->getEditFields())) ? $user->getEmail() : $user->getUsername());
		if ($returnValue != '') {array_push($errors, $returnValue);}
		// validate required fields
		$returnValue = $this->validateRequiredFields($user);
		if (!empty($returnValue)) {$errors = array_merge($errors, $returnValue);}
		// validate email repeating (if it has changed)
		if ($currentUserData['email'] != $user->getEmail())
		{
			if (in_array('emailRepeat', $user->getEditFields()))
			{
				$returnValue = $this->validateEmailRepeat($user->getEmail(), $user->getEmailRepeat());
				if ($returnValue != '') {array_push($errors, $returnValue);}
			}
		}
		// validate password rules
		if ($currentUserData['password'] != $user->getPassword())
		{
			$returnValue = $this->validatePasswordRules($user->getPassword());
			if ($returnValue != '') {array_push($errors, $returnValue);}
			// validate password repeat
			if (in_array('passwordRepeat', $user->getEditFields()))
			{
				$returnValue = $this->validatePasswordRepeat($user->getPassword(), $user->getPasswordRepeat());
				if ($returnValue != '') {array_push($errors, $returnValue);}
			}
		}
		// validate uploaded image
		if (in_array('image', $user->getEditFields()) && !empty($user->getPicture()['tmp_name']))
		{
			$returnValue = $this->validateImage($user, (!empty($errors)));
			if ($returnValue != '') {array_push($errors, $returnValue);}
		}
		// validate and set user data's
		$returnValue = $this->validateDatas($currentUserData, $user, (!empty($errors)));
		if ($returnValue != '') {array_push($errors, $returnValue);}
		// return result
		return $errors;
	}


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	/**
     * Validate on required empty fields
     *
     * @param	\Maagit\Maagituser\Domain\Model\User	$user			the registration form object
	 * @return	array													array with given error messages | empty array
     */
	function validateRequiredFields(\Maagit\Maagituser\Domain\Model\User $user)
	{
		$emptyFields = array();
		foreach ($user->getEditRequiredFields() as $key => $value)
		{
			if (in_array($key, $user->getEditFields()))
			{
				$value = $this->divHelper->getMemberByReflection($user, $key);
				if (empty($value))
				{
					$label = \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('mutation.'.$key, 'maagituser');
					array_push($emptyFields, \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('mutation.error.required.prefix', 'maagituser').'&laquo;'.$label.'&raquo '.\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('mutation.error.required.suffix', 'maagituser'));
				}
			}
		}
		return $emptyFields;
	}

	/**
     * Validate on unique username
     *
     * @param	string								$current			the current username
	 * @param	string								$new				the new username
	 * @return	string													error message | empty string
     */
	protected function validateUniqueUsername(string $current, string $new)
	{
		if (strtolower(trim($current)) == strtolower(trim($new))) {return;}
		if ($this->userRepository->findByEmail($new, true))
		{
			return \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('mutation.error.unique', 'maagituser');
		}
		return '';
	}

	/**
     * Validate on same email and email repeat
     *
     * @param	string								$email				the email
	 * @param	string								$emailRepeat		the repeated email
	 * @return	string													error message | empty string
     */
	protected function validateEmailRepeat(string $email, string $emailRepeat)
	{
		if ($email != $emailRepeat)
		{
			return \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('mutation.error.email.repeat', 'maagituser');
		}
		return '';
	}

	/**
     * Validate on correct password rules
     *
     * @param	string								$password			the password
	 * @return	string													error message | empty string
     */
	protected function validatePasswordRules(string $password)
	{
		if (!$this->passwordService->isValidPassword($password))
		{
			return \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('mutation.error.password.rules', 'maagituser');
		}
		return '';
	}

	/**
     * Validate on same password and password repeat
     *
     * @param	string								$password			the password
	 * @param	string								$passwordRepeat		the repeated password
	 * @return	string													error message | empty string
     */
	protected function validatePasswordRepeat(string $password, string $passwordRepeat)
	{
		if ($password != $passwordRepeat)
		{
			return \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('mutation.error.password.repeat', 'maagituser');
		}
		return '';
	}

	/**
     * Validate and handle uploaded file
     *
     * @param	\Maagit\Maagituser\Domain\Model\User		&$user			the user object
	 * @param	boolean										hasErrors		are there other errors occured?
	 * @return	string														error message | empty string
     */
	protected function validateImage(&$user, bool $hasErrors=false)
	{
		if (!in_array(pathinfo($user->getPicture()['name'])['extension'], explode(',', $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext'])))
		{
			return \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('mutation.error.file.extension', 'maagituser');
		}
		if (isset($this->settings['registrationMaxUploadSize']) && (int)$this->settings['mutationMaxUploadSize'] > 0)
		{
			if ((int)$user->getPicture()['size'] > (int)$this->settings['mutationMaxUploadSize'])
			{
				return \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('mutation.error.file.size', 'maagituser', array($this->settings['mutationMaxUploadSize'], $user->getPicture()['size']));
			}
		}
		if (!$hasErrors)
		{
			$fileService = $this->makeInstance('Maagit\Maagituser\Service\FileService');
			$uploadPath = (isset($this->settings['mutationUploadPath'])) ? $this->settings['mutationUploadPath'] : '';
			$uploadedFileResource = $fileService->saveUploadedFile($user->getPicture(), $uploadPath);
			if ($uploadedFileResource === false)
			{
				return \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('mutation.error.file.save', 'maagituser');
			}
			$fileReference = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('Maagit\Maagituser\Domain\Model\FileReference');
			$fileReference->setFile($uploadedFileResource);
			$fileReference->setPid($user->getPid());
			$user->setImage($fileReference);
		}
		return '';
	}

	/**
     * Validate and set various datas as username/email/password changed, ...
     *
     * @param	\Maagit\Maagituser\Domain\Model\User		$dbRecord		the database user object
	 * @param	\Maagit\Maagituser\Domain\Model\User		&$user			the edited user object
	 * @param	boolean										hasErrors		are there other errors occured?
	 * @return	string														error message | empty string
     */
	protected function validateDatas($dbRecord, &$user, bool $hasErrors=false)
	{
		if ($hasErrors) {return '';}
		// change username, if username and email is same and email has changed
		if ($dbRecord['username'] == $dbRecord['email'] && $dbRecord['username'] != $user->getEmail())
		{
			$user->setUsername((!in_array('username', $user->getEditFields())) ? trim($user->getEmail()) : trim($user->getUsername()));			
		}
		// set email changed
		if ($dbRecord['email'] != $user->getEmail())
		{
			$user->setEmailChanged(true);
		}
		// set password changed and encrypt it
		if ($dbRecord['password'] != $user->getPassword())
		{
			$user->setPasswordChanged(true);
			$user->setPassword($this->passwordService->getPasswordHash($user->getPassword()));
		}
		// set username changed
		if ($dbRecord['username'] != $user->getUsername())
		{
			$user->setUsernameChanged(true);
		}
		// set full name, if it's empty
		if (empty($dbRecord['name']) && (!empty($user->getFirstName()) && !empty($user->getLastName())))
		{
			$user->setName($user->getFirstName().' '.$user->getLastName());
		}
		return '';
	}


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}