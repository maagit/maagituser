<?php
namespace Maagit\Maagituser\Service;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2022-2022 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagituser
	Package:			Service
	class:				PasswordService

	description:		Password recovery methods.

	created:			2022-01-22
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2022-01-22	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class PasswordService extends \Maagit\Maagituser\Service\BaseService
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * Validate given password of configurated rules (see in typoscript)
     *
     * @param	string					$password			the password
	 * @return	boolean										is valid?
     */
	public function isValidPassword($password)
	{
        $passwordPolicy = $GLOBALS['TYPO3_CONF_VARS']['FE']['passwordPolicy'] ?? 'default';
        $passwordPolicyValidator = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\PasswordPolicy\PasswordPolicyValidator::class, \TYPO3\CMS\Core\PasswordPolicy\PasswordPolicyAction::NEW_USER_PASSWORD, is_string($passwordPolicy) ? $passwordPolicy : '');
		
		if (!$passwordPolicyValidator->isValidPassword($password))
		{
			return false;
		}
		return true;
	}
	
	/**
     * Return password hash of given password
     *
     * @param	string					$password			the password
	 * @return	string										the hash
     */
	public function getPasswordHash($password)
	{
		$passwordHashFactory = $this->makeInstance('TYPO3\CMS\Core\Crypto\PasswordHashing\PasswordHashFactory')->getDefaultHashInstance('FE');
        return $passwordHashFactory->getHashedPassword($password);
	}


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}