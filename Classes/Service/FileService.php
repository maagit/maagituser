<?php
namespace Maagit\Maagituser\Service;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2022-2022 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagituser
	Package:			Service
	class:				FileService

	description:		Resource and file handling methods.

	created:			2022-01-30
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2022-01-30	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class FileService extends \Maagit\Maagituser\Service\BaseService
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * Get uploaded file and save it to the file system
     *
     * @param	array						$fileInformation		the uploaded file
	 * @param	string						$path					the path to upload to
	 * @return	TYPO3\CMS\Core\Resource								the file resource|false
     */
	public function saveUploadedFile(array $fileInformation, string $path='')
	{
		$file = false;
		try
		{
			$folder = $this->getUploadFolder($path);
			$file = $folder->addUploadedFile($fileInformation, \TYPO3\CMS\Core\Resource\Enum\DuplicationBehavior::RENAME);	
		}
		catch (\Exception $ex) { }
		return $file;
	}
	
	/**
     * Get storage folder
     *
     * @param	string								$path			the path
	 * @return	TYPO3\CMS\Core\Resource\Folder						the folder resource|false
     */
	public function getUploadFolder(string $path='')
	{
		$storage = $this->makeInstance('TYPO3\CMS\Core\Resource\ResourceFactory')->getDefaultStorage();
		$path = ($path=='') ? '/user_upload/' : $path;
		if ($storage->hasFolder($path))
		{
			$folder = $storage->getFolder($path);
		}
		else
		{
			$folder = $storage->createFolder($path);
		}
		return $folder;
	}


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}