<?php
namespace Maagit\Maagituser\Domain\Repository;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2022-2022 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagituser
	Package:			Repository
	class:				UserRepository

	description:		Repository for the typo3 frontend users.

	created:			2022-01-21
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2022-01-21	Urs Maag		Initial version
						2022-10-10	Urs Maag		Typo3 12.0.0 compatibility
													- $this->objectType without leading slash
													- new method "getOldUserData" for
													  getting old saved values from user

------------------------------------------------------------------------------------- */


class UserRepository extends \Maagit\Maagituser\Domain\Repository\BaseRepository
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
    /**
     * @var \TYPO3\CMS\Core\Database\Connection
     */
    protected $connection;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	public function initializeObject()
	{
		parent::initializeObject();
		$this->objectType = 'Maagit\Maagituser\Domain\Model\User';
		$this->connection = $this->makeInstance('TYPO3\CMS\Core\Database\ConnectionPool')->getConnectionForTable($this->getUserTable());
	}


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * Search frontend user record with given email
     *
     * @param	string				$email			the email address
	 * @param	boolean				$allowHidden	also find hidden records?
	 * @return	boolean								user found?
     */
	public function findByEmail(string $email, bool $allowHidden=false)
	{
	    if (empty($email)) {return false;}
        $queryBuilder = $this->connection->createQueryBuilder();
        if ($allowHidden)
		{
			$queryBuilder->getRestrictions()->removeByType(\TYPO3\CMS\Core\Database\Query\Restriction\HiddenRestriction::class);	
		}
		$query = $queryBuilder->select('username')->from($this->getUserTable())->where($queryBuilder->expr()->eq('username', $queryBuilder->createNamedParameter($email)));
        if (!empty($this->settings['pages']))
		{
			$query->andWhere($queryBuilder->expr()->in('pid', $this->settings['pages']));
		}
        $column = $query->executeQuery()->fetchAssociative();
        return $column === false || $column === '' ? false : true;
	}

	/**
     * Search frontend user record with given verification hash
     *
     * @param	string				$hash			the verification hash
	 * @return	boolean								user found?
     */
	public function findByVerificationHash(string $hash)
	{
        if (empty($hash)) {return false;}
        $queryBuilder = $this->connection->createQueryBuilder();
        $queryBuilder->getRestrictions()->removeByType(\TYPO3\CMS\Core\Database\Query\Restriction\HiddenRestriction::class);
		$query = $queryBuilder->select('username')->from($this->getUserTable())->where($queryBuilder->expr()->eq('maagituser_verifyHash', $queryBuilder->createNamedParameter($hash)))->setMaxResults(1);
		if (!empty($this->settings['pages']))
		{
			$query->andWhere($queryBuilder->expr()->in('pid', $this->settings['pages']));
		}
        $row = $query->executeQuery()->fetchAssociative();
        return is_array($row) ? true : false;
	}

	/**
     * Search frontend user record with given uid
     *
     * @param	int					$uid			the uid
	 * @return	array								user record
     */
	public function getOldUserData(int $uid)
	{
        $queryBuilder = $this->connection->createQueryBuilder();
		$queryBuilder->getRestrictions()->removeByType(\TYPO3\CMS\Core\Database\Query\Restriction\HiddenRestriction::class);
		$query = $queryBuilder->select('*')->from($this->getUserTable())->where($queryBuilder->expr()->eq('uid', $queryBuilder->createNamedParameter($uid)));
		if (!empty($this->settings['pages']))
		{
			$query->andWhere($queryBuilder->expr()->in('pid', $this->settings['pages']));
		}
        $row = $query->executeQuery()->fetchAssociative();
        return $row;
	}

	/**
     * add user record to database
     *
     * @param	\Maagit\Maagituser\Domain\Model\User	$user			the user record
	 * @return	void
     */
	public function add($user)
	{
		$persistenceManager = $this->makeInstance('TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager');
		$passwordService = $this->makeInstance('Maagit\Maagituser\Service\PasswordService');
		$user->setPid($this->settings['registrationStoragePid']);
		$user->setUsergroup($this->settings['registrationUsergroup']);
		$user->setUsername(empty($user->getUsername()) ? trim($user->getEmail()) : trim($user->getUsername()));
		$user->setPassword($passwordService->getPasswordHash($user->getPassword()));
		if ($this->settings['registrationVerify']) {$user->setDisable(true);}
		parent::add($user);
		$persistenceManager->persistAll();
	}
	
	/**
     * update user record and save it to database
     *
     * @param	\Maagit\Maagituser\Domain\Model\User	$user			the user record
	 * @return	void
     */
	public function update($user)
	{
		$persistenceManager = $this->makeInstance('TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager');
		parent::update($user);
		$persistenceManager->persistAll();
	}

	/**
     * delete user
     *
     * @param	\Maagit\Maagituser\Domain\Model\User	$user			the user record
	 * @return	void
     */
	public function remove($user)
	{
		$persistenceManager = $this->makeInstance('TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager');
		parent::remove($user);
		$persistenceManager->persistAll();
	}

	/**
     * Set verification hash to given user
     *
     * @param	string				$user			the username
	 * @param	string				$hash			the hash
	 * @return	void
     */
	public function setVerifyHash(string $user, string $hash)
	{
		$hashService = $this->makeInstance('TYPO3\CMS\Core\Crypto\HashService');
		$queryBuilder = $this->connection->createQueryBuilder();
		$queryBuilder->getRestrictions()->removeByType(\TYPO3\CMS\Core\Database\Query\Restriction\HiddenRestriction::class);
		$query = $queryBuilder->update($this->getUserTable())->where($queryBuilder->expr()->eq('username',$queryBuilder->createNamedParameter($user)))->set('maagituser_verifyHash', $hashService->hmac($hash, 'x'));
		$query->executeQuery();
	}

	/**
     * Set new password hash and clear forgotHash
     *
     * @param	string				$passwordHash			the password hash
	 * @param	string				$hash					the password verify hash of recovery link
	 * @return	void
     */
	public function setPasswordHash(string $passwordHash, string $hash)
	{
		$queryBuilder = $this->connection->createQueryBuilder();
		$context = $this->makeInstance('TYPO3\CMS\Core\Context\Context');
		$timestamp = $context->getPropertyFromAspect('date', 'timestamp');
		$query = $queryBuilder->update($this->getUserTable())->set('password', $passwordHash)->set('maagituser_verifyHash', $this->connection->quote(''), false)->set('tstamp', $timestamp)->where($queryBuilder->expr()->eq('maagituser_verifyHash', $queryBuilder->createNamedParameter($hash)));
		$query->executeQuery();
	}
	
	/**
     * Set disable=false and clear hash to given user
     *
	 * @param	string				$verifyhash		the account verification hash of registration link
	 * @return	void
     */
	public function setActivateUser(string $hash)
	{
		$hashService = $this->makeInstance('TYPO3\CMS\Core\Crypto\HashService');
		$hash = $hashService->hmac($hash, 'x');
		$queryBuilder = $this->connection->createQueryBuilder();
		$queryBuilder->getRestrictions()->removeByType(\TYPO3\CMS\Core\Database\Query\Restriction\HiddenRestriction::class);
		$context = $this->makeInstance('TYPO3\CMS\Core\Context\Context');
		$timestamp = $context->getPropertyFromAspect('date', 'timestamp');
		$query = $queryBuilder->update($this->getUserTable())->set('disable', false)->set('maagituser_verifyHash', $this->connection->quote(''), false)->set('tstamp', $timestamp)->where($queryBuilder->expr()->eq('maagituser_verifyHash', $queryBuilder->createNamedParameter($hash)));
		$query->executeQuery();
	}

	/**
     * Set deleted=true and clear hash to given user
     *
	 * @param	string				$verifyhash		the account verification hash of registration link
	 * @return	void
     */
	public function setDeleteUser(string $hash)
	{
		$hashService = $this->makeInstance('TYPO3\CMS\Core\Crypto\HashService');
		$hash = $hashService->hmac($hash, 'x');
		$queryBuilder = $this->connection->createQueryBuilder();
		$context = $this->makeInstance('TYPO3\CMS\Core\Context\Context');
		$timestamp = $context->getPropertyFromAspect('date', 'timestamp');
		$query = $queryBuilder->update($this->getUserTable())->set('deleted', true)->set('maagituser_verifyHash', $this->connection->quote(''), false)->set('tstamp', $timestamp)->where($queryBuilder->expr()->eq('maagituser_verifyHash', $queryBuilder->createNamedParameter($hash)));
		$query->executeQuery();
	}


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	/**
     * Get typo3 tablename of frontend user's
     *
     * @param	-
	 * @return	string						the table name
     */
	protected function getUserTable()
	{
		$feUser = $this->makeInstance('TYPO3\\CMS\\Frontend\\Authentication\\FrontendUserAuthentication');
		return $feUser->user_table;
	}


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}
