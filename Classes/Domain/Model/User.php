<?php
namespace Maagit\Maagituser\Domain\Model;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2022-2022 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagituser
	Package:			Model
	class:				User

	description:		Model for the user
						Inherits datas for managing the user records.

	created:			2022-01-28
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2022-01-28	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class User extends \Maagit\Maagituser\Domain\Model\BaseModel
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var array
	 */
	protected $formFields;
	
	/**
	 * @var array
	 */
	protected $requiredFields;

	/**
	 * @var array
	 */
	protected $editFields;
	
	/**
	 * @var array
	 */
	protected $editRequiredFields;

	/**
	 * @var string
	 */
	protected $username;

	/**
	 * @var string
	 */
	protected $password;

	/**
	 * @var string
	 */
	protected $passwordRepeat;

	/**
	 * @var string
	 */
	protected $company;

	/**
	 * @var string
	 */
	protected $title;

	/**
	 * @var string
	 */
	protected $name;

	/**
	 * @var string
	 */
	protected $firstName;

	/**
	 * @var string
	 */
	protected $first_name;

	/**
	 * @var string
	 */
	protected $middleName;

	/**
	 * @var string
	 */
	protected $middle_name;

	/**
	 * @var string
	 */
	protected $lastName;

	/**
	 * @var string
	 */
	protected $last_name;

	/**
	 * @var string
	 */
	protected $address;

	/**
	 * @var string
	 */
	protected $zip;

	/**
	 * @var string
	 */
	protected $city;

	/**
	 * @var string
	 */
	protected $country;

	/**
	 * @var string
	 */
	protected $telephone;

	/**
	 * @var string
	 */
	protected $fax;

	/**
	 * @var string
	 */
	protected $email;

	/**
	 * @var string
	 */
	protected $emailRepeat;

	/**
	 * @var string
	 */
	protected $www;

	/**
	 * @var array
	 */
	protected $picture = array();

	/**
	 * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
	 */
	protected $image;
	
	/**
	 * @var string
	 */
	protected $usergroup;

	/**
	 * @var boolean
	 */
	protected $disable;

	/**
	 * @var boolean
	 */
	protected $usernameChanged = false;

	/**
	 * @var boolean
	 */
	protected $passwordChanged = false;
	
	/**
	 * @var boolean
	 */
	protected $emailChanged = false;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */
	/**
     * Returns the selected form fields
     *
     * @return array	$formFields
     */
    public function getFormFields()
    {
		return explode(',', $this->settings['registrationFormFields']);
    }

	/**
     * Returns the required fields
     *
     * @return array	$requiredFields
     */
    public function getRequiredFields()
    {
		$fields = array();
		foreach (explode(',', $this->settings['registrationFormFieldsRequired']) as $key => $value)
		{
			$fields[$value] = 1;
		}
        return $fields;
    }

	/**
     * Returns the selected edit form fields
     *
     * @return array	$editFields
     */
    public function getEditFields()
    {
		return explode(',', $this->settings['mutationFormFields']);
    }

	/**
     * Returns the required edit fields
     *
     * @return array	$editRequiredFields
     */
    public function getEditRequiredFields()
    {
		$fields = array();
		foreach (explode(',', $this->settings['mutationFormFieldsRequired']) as $key => $value)
		{
			$fields[$value] = 1;
		}
        return $fields;
    }

	/**
     * Sets the username
     *
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

	/**
     * Returns the username
     *
     * @return string	$username
     */
    public function getUsername()
    {
		return $this->username;
    }

	/**
     * Sets the password
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

	/**
     * Returns the password
     *
     * @return string	$password
     */
    public function getPassword()
    {
        return $this->password;
    }

	/**
     * Sets the passwordRepeat
     *
     * @param string $passwordRepeat
     */
    public function setPasswordRepeat($passwordRepeat)
    {
        $this->passwordRepeat = $passwordRepeat;
    }

	/**
     * Returns the passwordRepeat
     *
     * @return string	$passwordRepeat
     */
    public function getPasswordRepeat()
    {
        return $this->passwordRepeat;
    }

	/**
     * Sets the company
     *
     * @param string $company
     */
    public function setCompany($company)
    {
        $this->company = $company;
    }

	/**
     * Returns the company
     *
     * @return string	$company
     */
    public function getCompany()
    {
        return $this->company;
    }

	/**
     * Sets the title
     *
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

	/**
     * Returns the title
     *
     * @return string	$title
     */
    public function getTitle()
    {
        return $this->title;
    }

	/**
     * Sets the name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

	/**
     * Returns the name
     *
     * @return string	$name
     */
    public function getName()
    {
        return $this->name;
    }

	/**
     * Sets the firstName
     *
     * @param string $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
		$this->first_name = $firstName;
    }

	/**
     * Returns the firstName
     *
     * @return string	$firstName
     */
    public function getFirstName()
    {
        return $this->firstName;
    }
	
	/**
     * Sets the middleName
     *
     * @param string $middleName
     */
    public function setMiddleName($middleName)
    {
        $this->middleName = $middleName;
		$this->middle_name = $middleName;
    }

	/**
     * Returns the middleName
     *
     * @return string	$middleName
     */
    public function getMiddleName()
    {
        return $this->middleName;
	}

	/**
     * Sets the lastName
     *
     * @param string $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
		$this->last_name = $lastName;
    }

	/**
     * Returns the lastName
     *
     * @return string	$lastName
     */
    public function getLastName()
    {
        return $this->lastName;
    }

	/**
     * Sets the address
     *
     * @param string $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

	/**
     * Returns the address
     *
     * @return string	$address
     */
    public function getAddress()
    {
        return $this->address;
    }

	/**
     * Sets the zip
     *
     * @param string $zip
     */
    public function setZip($zip)
    {
        $this->zip = $zip;
    }

	/**
     * Returns the zip
     *
     * @return string	$zip
     */
    public function getZip()
    {
        return $this->zip;
    }

	/**
     * Sets the city
     *
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

	/**
     * Returns the city
     *
     * @return string	$city
     */
    public function getCity()
    {
        return $this->city;
    }

	/**
     * Sets the country
     *
     * @param string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

	/**
     * Returns the country
     *
     * @return string	$country
     */
    public function getCountry()
    {
        return $this->country;
    }

	/**
     * Sets the telephone
     *
     * @param string $telephone
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
    }

	/**
     * Returns the telephone
     *
     * @return string	$telephone
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

	/**
     * Sets the fax
     *
     * @param string $fax
     */
    public function setFax($fax)
    {
        $this->fax = $fax;
    }

	/**
     * Returns the fax
     *
     * @return string	$fax
     */
    public function getFax()
    {
        return $this->fax;
    }

	/**
     * Sets the email
     *
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

	/**
     * Returns the email
     *
     * @return string	$email
     */
    public function getEmail()
    {
        return $this->email;
    }

	/**
     * Sets the emailRepeat
     *
     * @param string $emailRepeat
     */
    public function setEmailRepeat($emailRepeat)
    {
        $this->emailRepeat = $emailRepeat;
    }

	/**
     * Returns the emailRepeat
     *
     * @return string	$emailRepeat
     */
    public function getEmailRepeat()
    {
        return $this->emailRepeat;
    }

	/**
     * Sets the www
     *
     * @param string $www
     */
    public function setWww($www)
    {
        $this->www = $www;
    }

	/**
     * Returns the www
     *
     * @return string	$www
     */
    public function getWww()
    {
        return $this->www;
    }

	/**
     * Sets the picture
     *
     * @param array	$picture
     */
    public function setPicture($picture)
    {
        $this->picture = $picture;
    }

	/**
     * Returns the picture
     *
     * @return array	$picture
     */
    public function getPicture()
    {
        return $this->picture;
    }

	/**
     * Sets the image
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference	$image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

	/**
     * Returns the image
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference	$image
     */
    public function getImage()
    {
        return $this->image;
    }

	/**
     * Sets the usergroup
     *
     * @param string $usergroup
     */
    public function setUsergroup($usergroup)
    {
        $this->usergroup = $usergroup;
    }

	/**
     * Returns the usergroup
     *
     * @return string	$usergroup
     */
    public function getUsergroup()
    {
        return $this->usergroup;
    }
	
	/**
     * Sets the disable
     *
     * @param boolean $disable
     */
    public function setDisable($disable)
    {
        $this->disable = $disable;
    }

	/**
     * Returns the disable
     *
     * @return boolean	$disable
     */
    public function getDisable()
    {
        return $this->disable;
    }

	/**
     * Sets the usernameChanged
     *
     * @param boolean $usernameChanged
     */
    public function setUsernameChanged($usernameChanged)
    {
        $this->usernameChanged = $usernameChanged;
    }

	/**
     * Returns the usernameChanged
     *
     * @return boolean	$usernameChanged
     */
    public function getUsernameChanged()
    {
        return $this->usernameChanged;
    }
	
	/**
     * Sets the passwordChanged
     *
     * @param boolean $passwordChanged
     */
    public function setPasswordChanged($passwordChanged)
    {
        $this->passwordChanged = $passwordChanged;
    }

	/**
     * Returns the passwordChanged
     *
     * @return boolean	$passwordChanged
     */
    public function getPasswordChanged()
    {
        return $this->passwordChanged;
    }

	/**
     * Sets the emailChanged
     *
     * @param boolean $emailChanged
     */
    public function setEmailChanged($emailChanged)
    {
        $this->emailChanged = $emailChanged;
    }

	/**
     * Returns the emailChanged
     *
     * @return boolean	$emailChanged
     */
    public function getEmailChanged()
    {
        return $this->emailChanged;
    }


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	
 
	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */

		
	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}