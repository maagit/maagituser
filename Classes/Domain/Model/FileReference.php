<?php
namespace Maagit\Maagituser\Domain\Model;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2022-2022 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagituser
	Package:			Model
	class:				FileReference

	description:		Model for users image file reference.

	created:			2022-01-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2022-01-29	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class FileReference extends \TYPO3\CMS\Extbase\Domain\Model\FileReference
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var integer
	 */
	protected $originalFileIdentifier;

	/**
	 * @var string
	 */
	protected $tableLocal = 'sys_file';

	/**
	 * @var string
	 */
	protected $tablenames = 'fe_users';

	/**
	 * @var integer
	 */
	protected $sortingForeign = 1;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * Set original resource
     *
     * @param	\TYPO3\CMS\Core\Resource\FileReference		$originalResource			the original resource
	 * @return	void
     */
	public function setOriginalResource(\TYPO3\CMS\Core\Resource\ResourceInterface $originalResource): void
	{
		$this->originalResource = $originalResource;
		$this->originalFileIdentifier = (int)$originalResource->getOriginalFile()->getUid();
	}	
 
	/**
     * Set file
     *
     * @param	\TYPO3\CMS\Core\Resource\File		$file			the file
	 * @return	void
     */
	public function setFile(\TYPO3\CMS\Core\Resource\File $file)
	{
		$this->originalFileIdentifier = (int)$file->getUid();
	}


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */

		
	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}