<?php
namespace Maagit\Maagituser\Helper;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2022-2022 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagituser
	Package:			Helper
	class:				DivHelper

	description:		Various helper methods for this plugin.

	created:			2022-01-08
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2022-01-08	Urs Maag		Initial version
						2023-12-13	Urs Maag		Added attribute "site" to request
													on getExtbaseRequest()

------------------------------------------------------------------------------------- */


class DivHelper extends \Maagit\Maagituser\Helper\BaseHelper
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	
	
	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * execute a object method by reflection
     *
     * @param	object		$object			the object to call the method
	 * @param	string		$member			the member name
	 * @return	mixed						the member variable
     */
	public function getMemberByReflection(object $object, string $member)
	{
		$reflectionClass = new \ReflectionClass($object);
		$reflectionProperty = $reflectionClass->getProperty($this->toLowerCamelCase($member));
		$reflectionProperty->setAccessible(true);
		return $reflectionProperty->getValue($object);
	}

	/**
     * translate repository name to model name
     *
     * @param	\Maagit\Maagituser\Domain\Repository\BaseRepository		$repository			the repository object
	 * @return	string																		the model name
     */
	public function getModelName(\Maagit\Maagituser\Domain\Repository\BaseRepository $repository)
	{
		$name = \TYPO3\CMS\Core\Utility\ClassNamingUtility::translateRepositoryNameToModelName(get_class($repository));
		return $name;
	}

	/**
     * convert string to camel case with first letter of string is lower case
     *
     * @param	string		$string			the string
	 * @return	string						the converted string
     */
	public function toLowerCamelCase(string $string)
	{
		$string = preg_replace_callback('/_(.)/', function($matches) {
			return strtoupper($matches[1]);
		}, $string);
		return $string;
	}

	/**
     * create a core request object
     *
     * @param	-
	 * @return	\TYPO3\CMS\Core\Http\ServerRequestInterface					the request object
     */
	public function getRequest()
	{
	    return \TYPO3\CMS\Core\Http\ServerRequestFactory::fromGlobals();
	}

	/**
     * create a extbase request object
     *
     * @param	string								$controller		the controller class as string
	 * @return	\TYPO3\CMS\Extbase\Mvc\Request						the extbase request object
     */
	public function getExtbaseRequest(string $controller='\Maagit\Maagituser\Controller\LoginController')
	{
		$siteFinder = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Site\\SiteFinder');
		$sites = $siteFinder->getAllSites();
		$site = empty($sites['default']) ? ($sites['config'] ?? array_values($sites)[0]) : $sites['default'];
		// @extensionScannerIgnoreLine
		$contentObject = $this->getContentObject();
		$pageInformation = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Frontend\Page\PageInformation::class);
		$pageInformation->setId($contentObject?->data['pid']);
		$extbaseAttribute = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Mvc\ExtbaseRequestParameters::class, $controller);
		$extbaseRequest = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Mvc\Request::class, $this->getRequest()->withAttribute('extbase', $extbaseAttribute)->withAttribute('applicationType', 1)->withAttribute('site', $site)->withAttribute('currentContentObject', $contentObject)->withAttribute('frontend.page.information', $pageInformation));
		return $extbaseRequest;
	}

	/**
     * create a rendering context
     *
     * @param	-
	 * @return	\TYPO3\CMS\Extbase\Mvc\Request						the extbase request object
     */
	public function getRenderingContext()
	{
	    $renderingContext = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Fluid\Core\Rendering\RenderingContextFactory::class)->create();
	    $renderingContext->setRequest($this->getExtbaseRequest());
		return $renderingContext;
	}


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	/**
     * Get plugin as contentObject
     *
	 * @param	string				$plugin					the plugin name
	 * @return	array										the flexform values
	 */
	protected function getContentObject(string $plugin='maagituser_login')
	{
		$queryBuilder = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Database\ConnectionPool::class)->getQueryBuilderForTable('tt_content');	
		$queryBuilder 
			->select('tt_content.*')
			->from('tt_content')
			->where(
				$queryBuilder->expr()->in(
					'tt_content.CType', 
					$queryBuilder->createNamedParameter(array($plugin), \TYPO3\CMS\Core\Database\Connection::PARAM_STR_ARRAY)
				)
		);
		$record = $queryBuilder->executeQuery()->fetchAssociative();
		$contentObject = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer::class);
		if ($record !== FALSE)
		{
			$contentObject->start($record, 'tt_content');
		}
		return $contentObject;
	}


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}
