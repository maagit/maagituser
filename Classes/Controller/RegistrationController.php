<?php
namespace Maagit\Maagituser\Controller;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2022-2022 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagituser
	Package:			Controller
	class:				LoginController

	description:		Main class for the login.
						Process the given actions.

	created:			2022-01-28
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2022-01-28	Urs Maag		Initial version
						2022-10-10	Urs Maag		Make Typo3 12.0.0 compatible
													- return "ForwardResponse" on actions
													- remove "objectManager"

------------------------------------------------------------------------------------- */


class RegistrationController extends \Maagit\Maagituser\Controller\BaseController
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var \Maagit\Maagituser\Domain\Repository\UserRepository
     */
    protected $userRepository;

	/**
	 * @var \Maagit\Maagituser\Service\CryptoService
     */
    protected $cryptoService;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	/**
     * Constructor, initialize objects
     *
     * @return void
     */
	public function initializeObject()
	{
		// inject repositories
		$this->userRepository = $this->makeInstance('Maagit\Maagituser\Domain\Repository\UserRepository');
		$this->cryptoService = $this->makeInstance('Maagit\Maagituser\Service\CryptoService');

		// get page uids of user storage
		if (!empty($this->settings['pages']))
		{
			$pageRepository = $this->makeInstance('Maagit\Maagituser\Domain\Repository\PageRepository');
			$this->settings['pages'] = implode(',', $pageRepository->getPageTreeUids(\TYPO3\CMS\Core\Utility\GeneralUtility::TrimExplode(',', $this->settings['pages']), (int)$this->settings['recursive']));	
		}
	}


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * Action for this controller.
	 * show login form.
     *
     * @param	\Maagit\Maagituser\Domain\Model\User		$user			the user object
	 * @param	array										$errors			errors occured on validating the registration
	 * @return	void
     */
    public function showAction(\Maagit\Maagituser\Domain\Model\User $user=null, array $errors=array())
    {
		if ($user == null) {$user = $this->userRepository->create();}
		$this->view->assignMultiple(array(
			'user' => $user,
			'errors' => $errors
		));
		return $this->responseFactory->createResponse()->withAddedHeader('Content-Type', 'text/html; charset=utf-8')->withBody($this->streamFactory->createStream($this->view->render()));
    }

	/**
     * Action for this controller.
	 * validate data and save new user.
     *
     * @param	\Maagit\Maagituser\Domain\Model\User		$user			the user object
	 * @return	void
     */
    public function registerAction(\Maagit\Maagituser\Domain\Model\User $user)
    {
		$registrationService = $this->makeInstance('Maagit\Maagituser\Service\RegistrationService');
		$errors = $registrationService->validate($user);
		if (!empty($errors)) {return (new \TYPO3\CMS\Extbase\Http\ForwardResponse('show'))->withControllerName('Registration')->withArguments(array('errors' => $errors));}
		$this->userRepository->add($user);
		if ($this->settings['registrationVerify'])
		{
			try
			{
				$verifyHash = $this->cryptoService->sendLink($user->getEmail(), 'registration');
				$this->userRepository->setVerifyHash($user->getEmail(), $verifyHash);
			}
			catch (\Exception $ex)
			{
				return (new \TYPO3\CMS\Extbase\Http\ForwardResponse('show'))->withControllerName('Registration')->withArguments(array('errors' => $ex->getMessage()));
			}
		}
		if (!empty($this->settings['registrationRedirectPidAfterSave']))
		{
			return $this->redirect(null, null, null, null, $this->settings['registrationRedirectPidAfterSave']);	
		}
		$this->view->assignMultiple(array(
			'user' => $user
		));
		return $this->responseFactory->createResponse()->withAddedHeader('Content-Type', 'text/html; charset=utf-8')->withBody($this->streamFactory->createStream($this->view->render()));
    }
	
	/**
     * Action for this controller.
	 * validate verification hash and activate user.
     *
     * @param	\Maagit\Maagituser\Domain\Model\User		$user			the user object
	 * @return	void
     */
    public function activateAction()
    {
		$hash = $this->request->hasArgument('hash') ? $this->request->getArgument('hash') : '';
        $error = false;
		if (!$this->cryptoService->validateVerificationLink($hash))
		{
			$error = true;
		}
		else
		{
			$this->userRepository->setActivateUser($hash);
		}
		if (!empty($this->settings['registrationRedirectPidAfterConfirmation']))
		{
			return $this->redirect(null, null, null, null, $this->settings['registrationRedirectPidAfterConfirmation'], array('error' => $error));	
		}
		$this->view->assignMultiple(array(
			'error' => $error
		));
		return $this->responseFactory->createResponse()->withAddedHeader('Content-Type', 'text/html; charset=utf-8')->withBody($this->streamFactory->createStream($this->view->render()));
	}


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}