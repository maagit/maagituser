<?php
namespace Maagit\Maagituser\Controller;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2022-2022 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagituser
	Package:			Controller
	class:				DeleteController

	description:		Main class for deleting accounts.
						Process the given actions.

	created:			2022-02-12
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2022-02-12	Urs Maag		Initial version
						2022-10-10	Urs Maag		Make Typo3 12.0.0 compatible
													- return "ForwardResponse" on actions
													- remove "objectManager"

------------------------------------------------------------------------------------- */


class DeleteController extends \Maagit\Maagituser\Controller\BaseController
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var \TYPO3\CMS\Core\Context\UserAspect
     */
    protected $userAspect;

	/**
	 * @var \Maagit\Maagituser\Domain\Repository\UserRepository
     */
    protected $userRepository;

	/**
	 * @var \Maagit\Maagituser\Service\CryptoService
     */
    protected $cryptoService;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	/**
     * Constructor, initialize objects
     *
     * @return void
     */
	public function initializeObject()
	{
		// inject repositories and services
		$this->userRepository = $this->makeInstance('Maagit\Maagituser\Domain\Repository\UserRepository');
		$this->userAspect = $this->makeInstance('TYPO3\CMS\Core\Context\Context')->getAspect('frontend.user');
		$this->cryptoService = $this->makeInstance('Maagit\Maagituser\Service\CryptoService');

		// get page uids of user storage
		if (!empty($this->settings['pages']))
		{
			$pageRepository = $this->makeInstance('Maagit\Maagituser\Domain\Repository\PageRepository');
			$this->settings['pages'] = implode(',', $pageRepository->getPageTreeUids(\TYPO3\CMS\Core\Utility\GeneralUtility::TrimExplode(',', $this->settings['pages']), (int)$this->settings['recursive']));	
		}
	}


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * Action for this controller.
	 * show login form.
     *
     * @param	\Maagit\Maagituser\Domain\Model\User		$user			the user object
	 * @param	array										$errors			errors occured on saving the data's
	 * @return	void
     */
    public function showAction(\Maagit\Maagituser\Domain\Model\User $user=null, array $errors=array())
    {
		$errors = array();
		$user = $this->getUser($errors);
		$this->view->assignMultiple(array(
			'user' => ($user == null) ? array() : $user,
			'errors' => $errors
		));
		return $this->responseFactory->createResponse()->withAddedHeader('Content-Type', 'text/html; charset=utf-8')->withBody($this->streamFactory->createStream($this->view->render()));
    }

	/**
     * Action for this controller.
	 * delete account.
     *
     * @param	-
	 * @return	void
     */
    public function deleteAction()
    {
		$errors = array();
		$user = $this->getUser($errors);
		if ($user == null || !empty($errors)) {return (new \TYPO3\CMS\Extbase\Http\ForwardResponse('show'))->withArguments($errors);}
		if ($this->settings['deleteVerify'])
		{
			try
			{
				$dbRecord = $this->userRepository->findByUid($user->getUid());
				$verifyHash = $this->cryptoService->sendLink($user->getEmail(), 'delete');
				$this->userRepository->setVerifyHash($dbRecord->getEmail(), $verifyHash);
			}
			catch (\Exception $ex)
			{
				return (new \TYPO3\CMS\Extbase\Http\ForwardResponse('show'))->withArguments(array('errors' => $ex->getMessage()));
			}
		}
		else
		{
			$this->userRepository->remove($user);
			$frontendAuthentication = $this->makeInstance('TYPO3\CMS\Frontend\Authentication\FrontendUserAuthentication');
			$frontendAuthentication->start($this->request);
			$frontendAuthentication->logoff();
			if (!empty($this->settings['deleteRedirectPidAfterDelete']))
			{
				return $this->redirect(null, null, null, null, $this->settings['deleteRedirectPidAfterDelete']);
			}
		}
		return $this->redirect('confirm', 'Delete', null, array());
    }

	/**
     * Action for this controller.
	 * show confirmation message after deleting user account
     *
     * @param	-
	 * @return	void
     */
    public function confirmAction()
    {
		$this->view->assignMultiple(array(
			
		));
		return $this->responseFactory->createResponse()->withAddedHeader('Content-Type', 'text/html; charset=utf-8')->withBody($this->streamFactory->createStream($this->view->render()));
    }

	/**
     * Action for this controller.
	 * validate verification hash and delete user.
     *
     * @param	-
	 * @return	void
     */
    public function deleteConfirmedAction()
    {
		$hash = $this->request->hasArgument('hash') ? $this->request->getArgument('hash') : '';

        $error = false;
		if (!$this->cryptoService->validateVerificationLink($hash))
		{
			$error = true;
		}
		else
		{
			$this->userRepository->setDeleteUser($hash);
		}
		if (!empty($this->settings['deleteRedirectPidAfterConfirmation']))
		{
			return $this->redirect(null, null, null, null, $this->settings['deleteRedirectPidAfterConfirmation']);
		}
		$this->view->assignMultiple(array(
			'error' => $error
		));
		return $this->responseFactory->createResponse()->withAddedHeader('Content-Type', 'text/html; charset=utf-8')->withBody($this->streamFactory->createStream($this->view->render()));
	}


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	/**
     * Check, if there is a user logged in
     *
     * @param	-
	 * @return	boolean					user logged in
     */
	protected function isLoggedIn()
	{
		return $this->userAspect->isLoggedIn();
	}


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
	/**
     * Get logged in user and returns the user model or null
     *
     * @param	array										$errors			errors array
	 * @return	\Maagit\Maagituser\Domain\Model\User						the user | null
     */
	private function getUser(array &$errors)
	{
		$user = null;
		if ($this->isLoggedIn())
		{
			$user = $this->userRepository->findByUid($this->userAspect->get('id'));
			if ($user != null)
			{
				$user->setSettings($this->settings);
			}
			else
			{
				array_push($errors, \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('delete.error.notfound', 'maagituser'));
			}
		}
		if (!$this->isLoggedIn())
		{
			array_push($errors, \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('delete.error.notloggedin', 'maagituser'));
		}
		return $user;
	}
}