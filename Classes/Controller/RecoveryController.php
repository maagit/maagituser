<?php
namespace Maagit\Maagituser\Controller;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2022-2022 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagituser
	Package:			Controller
	class:				RecoveryController

	description:		Main class for the password recovery.
						Process the given actions.

	created:			2022-01-20
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2022-01-20	Urs Maag		Initial version
						2022-10-10	Urs Maag		Make Typo3 12.0.0 compatible
													- return "ForwardResponse" on actions
													- remove "objectManager"

------------------------------------------------------------------------------------- */


class RecoveryController extends \Maagit\Maagituser\Controller\BaseController
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var \Maagit\Maagituser\Service\CryptoService
     */
    protected $cryptoService;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	/**
     * Constructor, initialize objects
     *
     * @return void
     */
	public function initializeObject()
	{
		// inject services
		$this->cryptoService = $this->makeInstance('Maagit\Maagituser\Service\CryptoService');
		
		// get page uids of user storage
		if (!empty($this->settings['pages']))
		{
			$pageRepository = $this->makeInstance('Maagit\Maagituser\Domain\Repository\PageRepository');
			$this->settings['pages'] = implode(',', $pageRepository->getPageTreeUids(\TYPO3\CMS\Core\Utility\GeneralUtility::TrimExplode(',', $this->settings['pages']), (int)$this->settings['recursive']));	
		}
	}


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * Action for this controller.
	 * show login form.
     *
     * @param	string			$email			the email address
	 * @param	bool			$error			is there a error occured?
	 * @param	string			$message		additional error message
	 * @return	void
     */
    public function showAction(string $email='', bool $error=false, string $message='')
    {
		$this->view->assignMultiple(array(
			'email' => $email,
			'error' => $error,
			'message' => $message
		));
		return $this->responseFactory->createResponse()->withAddedHeader('Content-Type', 'text/html; charset=utf-8')->withBody($this->streamFactory->createStream($this->view->render()));
    }

	/**
     * Action for this controller.
	 * send recovery link.
     *
     * @param	-
	 * @return	void
     */
    public function sendAction()
    {
		$email = (isset($_POST['email'])) ? $_POST['email'] : '';
		$userRepository = $this->makeInstance('Maagit\Maagituser\Domain\Repository\UserRepository');
		if (!$userRepository->findByEmail($email))
		{
			$error = (isset($this->settings['showUserNotFoundMessage'])) ? $this->settings['showUserNotFoundMessage'] : false;
			return $this->redirect('show', 'Recovery', NULL, array('email' => $email, 'error' => $error));	
		}
		else
		{
			try
			{
				$userRepository->setVerifyHash($email, $this->cryptoService->sendLink($email, 'recovery'));
			}
			catch (\Exception $ex)
			{
				return $this->redirect('show', 'Recovery', NULL, array('email' => $email, 'error' => true, 'message' => $ex->getMessage()));	
			}
		}
		if (!empty($this->settings['recoveryRedirectPidAfterForgotLinkSended']))
		{
			return $this->redirect(null, null, null, null, $this->settings['recoveryRedirectPidAfterForgotLinkSended']);	
		}
		$this->view->assignMultiple(array(
			'email' => $email
		));
		return $this->responseFactory->createResponse()->withAddedHeader('Content-Type', 'text/html; charset=utf-8')->withBody($this->streamFactory->createStream($this->view->render()));
    }

	/**
     * Action for this controller.
	 * show new password form.
     *
     * @param	string				$hash				the hash (if on password change is a error occured)
	 * @param	boolean				$error				is on changing password a error occured?
	 * @return	void
     */
    public function showChangeAction(string $hash=null, bool $error=false)
    {
        $hash = $this->request->hasArgument('hash') ? $this->request->getArgument('hash') : '';
		if (!$this->cryptoService->validateVerificationLink($hash))
		{
			return $this->redirect('show', 'Recovery', NULL, array());	
		}
		$this->view->assignMultiple(array(
			'hash' => $hash,
			'error' => $error
		));
		return $this->responseFactory->createResponse()->withAddedHeader('Content-Type', 'text/html; charset=utf-8')->withBody($this->streamFactory->createStream($this->view->render()));
    }

	/**
     * Action for this controller.
	 * update changed password.
     *
     * @param	-
	 * @return	void
     */
    public function changeAction(string $hash, string $newPass, string $newPassRepeat)
    {
		$recoveryService = $this->makeInstance('Maagit\Maagituser\Service\RecoveryService');
		$result = $recoveryService->setRecoveredPassword($hash, $newPass, $newPassRepeat);
		if (!$result)
		{
			return $this->redirect('showChange', 'Recovery', NULL, array('hash' => $hash, 'error' => true));
		}
		if (!empty($this->settings['recoveryRedirectPidAfterPasswordChange']))
		{
			return $this->redirect(null, null, null, null, $this->settings['recoveryRedirectPidAfterPasswordChange']);	
		}
		return $this->responseFactory->createResponse()->withAddedHeader('Content-Type', 'text/html; charset=utf-8')->withBody($this->streamFactory->createStream($this->view->render()));
    }


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}