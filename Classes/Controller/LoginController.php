<?php
namespace Maagit\Maagituser\Controller;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2022-2022 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagituser
	Package:			Controller
	class:				LoginController

	description:		Main class for the login.
						Process the given actions.

	created:			2022-01-08
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2022-01-08	Urs Maag		Initial version
						2022-10-10	Urs Maag		Make Typo3 12.0.0 compatible
													- return "ForwardResponse" on actions
													- remove "objectManager"
													- add "requestToken" to showAction

------------------------------------------------------------------------------------- */


class LoginController extends \Maagit\Maagituser\Controller\BaseController
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
    /**
     * @var \TYPO3\CMS\Core\Context\UserAspect
     */
    protected $userAspect;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	/**
     * Constructor, initialize objects
     *
     * @return void
     */
	public function initializeObject()
	{
		// inject user aspect
		$this->userAspect = $this->makeInstance('TYPO3\CMS\Core\Context\Context')->getAspect('frontend.user');

		// get page uids of user storage
		if (!empty($this->settings['pages']))
		{
			$pageRepository = $this->makeInstance('Maagit\Maagituser\Domain\Repository\PageRepository');
			$this->settings['pages'] = implode(',', $pageRepository->getPageTreeUids(\TYPO3\CMS\Core\Utility\GeneralUtility::TrimExplode(',', $this->settings['pages']), (int)$this->settings['recursive']));	
		}
	}


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * Action for this controller.
	 * show login form.
     *
     * @param	boolean			$isRedirect		call it from a other action?
	 * @param	string			$username		the username
	 * @param	int				$permalogin		the permalogin state
	 * @param	boolean			$error			is there a login error occured?
	 * @return	void
     */
    public function showAction(bool $isRedirect=false, string $username='', int $permalogin=null, bool $error=false)
    {
		$this->view->assignMultiple(array(
			'isRedirect' => $isRedirect,
			'loggedIn' => $this->isLoggedIn(),
			'user' => ($this->isLoggedIn()) ? $this->request->getAttribute('frontend.user')->user : array(),
			'username' => $username,
			'permalogin' => $permalogin,
			'error' => $error,
			'lifetimeFailed' => ($GLOBALS['TYPO3_CONF_VARS']['FE']['lifetime'] <= 0) ? true : false,
			'requestToken' => \TYPO3\CMS\Core\Security\RequestToken::create('core/user-auth/fe')->withMergedParams(['pid' => $this->settings['pages']]),
		));
		return $this->responseFactory->createResponse()->withAddedHeader('Content-Type', 'text/html; charset=utf-8')->withBody($this->streamFactory->createStream($this->view->render()));
    }
	
	/**
     * Action for this controller.
	 * login.
     *
     * @param	-
	 * @return	void
     */
    public function loginAction()
    {
		if ($this->hasLoginErrorOccurred())
		{
			$user = (isset($_POST['user'])) ? $_POST['user'] : '';
			$permalogin = (isset($_POST['permalogin'])) ? $_POST['permalogin'] : null;
			return $this->redirect('show', 'Login', NULL, array('isRedirect' => true, 'username' => $user, 'permalogin' => $permalogin, 'error' => $this->hasLoginErrorOccurred()));
		}
		else
		{
			return $this->redirect('show', 'Login', NULL, array());
		}
		return $this->responseFactory->createResponse()->withAddedHeader('Content-Type', 'text/html; charset=utf-8')->withBody($this->streamFactory->createStream($this->view->render()));
    }

	/**
     * Action for this controller.
	 * logout.
     *
     * @param	-
	 * @return	void
     */
    public function logoutAction()
    {
		return $this->redirect('show', 'Login', NULL, array());
    }


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	/**
     * Check, if there is a user logged in
     *
     * @param	-
	 * @return	boolean					user logged in
     */
	protected function isLoggedIn()
	{
		return $this->userAspect->isLoggedIn();
	}

	/**
     * Check, if there is a login failure occured
     *
     * @param	-
	 * @return	boolean					error occured
     */
	protected function hasLoginErrorOccurred()
	{
		return !$this->userAspect->isLoggedIn();
	}


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}