<?php
namespace Maagit\Maagituser\Controller;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2022-2022 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagituser
	Package:			Controller
	class:				MutationController

	description:		Main class for the editing users.
						Process the given actions.

	created:			2022-01-31
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2022-01-31	Urs Maag		Initial version
						2022-10-10	Urs Maag		Make Typo3 12.0.0 compatible
													- return "ForwardResponse" on actions
													- remove "objectManager"
													- get picture argument in "saveAction"
						2024-09-22	Urs Maag		Remove picture argument from
													property mapping

------------------------------------------------------------------------------------- */


class MutationController extends \Maagit\Maagituser\Controller\BaseController
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var \Maagit\Maagituser\Domain\Repository\UserRepository
     */
    protected $userRepository;

	/**
	 * @var \TYPO3\CMS\Core\Context\UserAspect
     */
    protected $userAspect;

	/**
	 * @var \Maagit\Maagituser\Service\CryptoService
     */
    protected $cryptoService;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	/**
     * Constructor, initialize objects
     *
     * @return void
     */
	public function initializeObject()
	{
		// inject repositories
		$this->userRepository = $this->makeInstance('Maagit\Maagituser\Domain\Repository\UserRepository');
		$this->userAspect = $this->makeInstance('TYPO3\CMS\Core\Context\Context')->getAspect('frontend.user');
		$this->cryptoService = $this->makeInstance('Maagit\Maagituser\Service\CryptoService');

		// get page uids of user storage
		if (!empty($this->settings['pages']))
		{
			$pageRepository = $this->makeInstance('Maagit\Maagituser\Domain\Repository\PageRepository');
			$this->settings['pages'] = implode(',', $pageRepository->getPageTreeUids(\TYPO3\CMS\Core\Utility\GeneralUtility::TrimExplode(',', $this->settings['pages']), (int)$this->settings['recursive']));	
		}
	}


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * Action for this controller.
	 * show login form.
     *
     * @param	\Maagit\Maagituser\Domain\Model\User		$user			the user object
	 * @param	array										$errors			errors occured on saving the data's
	 * @return	void
     */
    public function showAction(\Maagit\Maagituser\Domain\Model\User $user=null, array $errors=array())
    {
		if ($this->isLoggedIn() && $user == null)
		{
			$user = $this->userRepository->findByUid($this->userAspect->get('id'));
			if ($user != null) {$user->setSettings($this->settings);}
		}
		if ($this->isLoggedIn() && $user == null)
		{
			array_push($errors, \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('mutation.error.notfound', 'maagituser'));
		}
		if (!$this->isLoggedIn())
		{
			array_push($errors, \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('mutation.error.notloggedin', 'maagituser'));
		}
		$this->view->assignMultiple(array(
			'loggedIn' => $this->isLoggedIn(),
			'user' => ($user == null) ? array() : $user,
			'errors' => $errors
		));
		return $this->responseFactory->createResponse()->withAddedHeader('Content-Type', 'text/html; charset=utf-8')->withBody($this->streamFactory->createStream($this->view->render()));
    }
	
	/**
	 * initialize save action
	 * Exclude property "picture" from property mapping
	 *
	 * @return void
	 */
	public function initializeSaveAction() 
	{
		if ($this->arguments->hasArgument('user')) {
			$this->arguments->getArgument('user')->getPropertyMappingConfiguration()->skipProperties('picture');
		}
	}

	/**
     * Action for this controller.
	 * validate data and save it.
     *
     * @param	\Maagit\Maagituser\Domain\Model\User		$user			the user object
	 * @return	void
     */
    public function saveAction(\Maagit\Maagituser\Domain\Model\User $user)
    {
		$mutationService = $this->makeInstance('Maagit\Maagituser\Service\MutationService');
		$user->setSettings($this->settings);

		if (count($_FILES) > 0 && !empty($_FILES['tx_maagituser_mutation']['name']['user']['picture']))
		{
			$file = array();
			foreach ($_FILES['tx_maagituser_mutation'] as $key => $value)
			{
				$file[$key] = $value['user']['picture'];
			}
			$user->setPicture($file);
		}

		$errors = $mutationService->validate($user);
		$disable = false;
		if (!empty($errors)) {return (new \TYPO3\CMS\Extbase\Http\ForwardResponse('show'))->withControllerName('Mutation')->withArguments(array('errors' => $errors));}
		if ($user->getEmailChanged() && $this->settings['mutationVerify'])
		{
			try
			{
				$dbRecord = $this->userRepository->getOldUserData($user->getUid());
				$verifyHash = $this->cryptoService->sendLink($user->getEmail(), 'mutation');
				$this->userRepository->setVerifyHash($dbRecord['email'], $verifyHash);
			}
			catch (\Exception $ex)
			{
				return (new \TYPO3\CMS\Extbase\Http\ForwardResponse('show'))->withControllerName('Mutation')->withArguments(array('errors' => $ex->getMessage()));
			}
			$disable = true;
		}
		$this->userRepository->update($user);
		if ($user->getUsernameChanged() || $user->getPasswordChanged() || $user->getEmailChanged())
		{
			$frontendAuthentication = $this->makeInstance('TYPO3\CMS\Frontend\Authentication\FrontendUserAuthentication');
			$frontendAuthentication->start($this->request);
			$frontendAuthentication->logoff();
		}
		if (!empty($this->settings['mutationRedirectPidAfterSave']))
		{
			return $this->redirect(null, null, null, null, $this->settings['mutationRedirectPidAfterSave']);
		}
		return $this->redirect('confirm', 'Mutation', null, array('user' => $user, 'disable' => $disable));
    }

	/**
     * Action for this controller.
	 * show confirmation message after saving user data
     *
     * @param	\Maagit\Maagituser\Domain\Model\User		$user			the user object
	 * @param	boolean										$disable		disable the user?
	 * @return	void
     */
    public function confirmAction(\Maagit\Maagituser\Domain\Model\User $user, bool $disable)
    {
		if ($disable)
		{
			$user->setDisable(true);
			$this->userRepository->update($user);
		}
		$this->view->assignMultiple(array(
			'loggedIn' => $this->isLoggedIn(),
			'user' => ($user == null) ? array() : $user
		));
		return $this->responseFactory->createResponse()->withAddedHeader('Content-Type', 'text/html; charset=utf-8')->withBody($this->streamFactory->createStream($this->view->render()));
    }

	/**
     * Action for this controller.
	 * validate verification hash and activate user.
     *
     * @param	\Maagit\Maagituser\Domain\Model\User		$user			the user object
	 * @return	void
     */
    public function activateAction()
    {
		$hash = $this->request->hasArgument('hash') ? $this->request->getArgument('hash') : '';
        $error = false;
		if (!$this->cryptoService->validateVerificationLink($hash))
		{
			$error = true;
		}
		else
		{
			$this->userRepository->setActivateUser($hash);
		}
		if (!empty($this->settings['mutationRedirectPidAfterConfirmation']))
		{
			return $this->redirect(null, null, null, null, $this->settings['mutationRedirectPidAfterConfirmation'], array('error' => $error));	
		}
		$this->view->assignMultiple(array(
			'error' => $error
		));
		return $this->responseFactory->createResponse()->withAddedHeader('Content-Type', 'text/html; charset=utf-8')->withBody($this->streamFactory->createStream($this->view->render()));
	}


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	/**
     * Check, if there is a user logged in
     *
     * @param	-
	 * @return	boolean					user logged in
     */
	protected function isLoggedIn()
	{
		return $this->userAspect->isLoggedIn();
	}


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}