<?php
namespace Maagit\Maagituser;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2022-2022 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagituser
	Package:			Trait
	class:				General

	description:		General trait (base class) for this extension.
						Inherits a "makeInstance" method which makes sure, that every
						object becomes the extension configuration settings in the
						member variable "$this->settings".
						Instantiates the the "configurationManager" class

	created:			2022-01-08
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-01-08	Urs Maag		Initial version
						2022-10-11	Urs Maag		Typo3 12.0.0 compatibility
													- add request variable to make
													  current request available on all
													  over "$this->makeInstance" generated
													  Maagit classes

------------------------------------------------------------------------------------- */


trait General 
{
	/* ======================================================================================= */
	/* C L A S S   S E T T I N G S                                                             */
	/* ======================================================================================= */
	/**
	 * @var array
     */
	protected $plugins = array(
		'maagituser_login',
		'maagituser_recovery',
		'maagituser_registration'
	);


	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var \TYPO3\CMS\Extbase\Configuration\ConfigurationManager
     */
	protected $configManager;
	
	/**
	 * @var \Maagit\Maagituser\Helper\DivHelper
     */
	protected $divHelper;
	
	/**
	 * @var array
     */
    protected array $settings;
	
	/**
	 * @var array
     */
    protected $configuration;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	public function __construct()
	{
		// get divHelper object
		$this->divHelper = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('Maagit\\Maagituser\\Helper\\DivHelper');

		// get object- and configration manager / instantiate services
		$this->configManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Configuration\\ConfigurationManagerInterface');
		$typoScriptService = $this->makeInstance('TYPO3\\CMS\\Core\\TypoScript\\TypoScriptService');

		// get typoscript configuration
		$tsConf = $this->configManager->getConfiguration(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_FULL_TYPOSCRIPT);
		$tsConf = $typoScriptService->convertTypoScriptArrayToPlainArray($tsConf);
		$this->settings = $tsConf['plugin']['tx_maagituser']['settings'];
		unset($tsConf['plugin']['tx_maagituser']['settings']);
		$this->configuration = $tsConf['plugin']['tx_maagituser'];

		// get flexform configuration
		$this->getFlexformSettings();
	}


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */
	/**
     * Sets the settings
     *
     * @param array $settings
     */
    public function setSettings($settings)
    {
        $this->settings = $settings;
    }
	
	/**
     * Returns the settings
     *
     * @return array	the typoscript settings
     */
    public function getSettings()
    {
        return $this->settings;
    }


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * Make instance of given class and return the object
     *
	 * @param	$class						string				classname to instantiate
	 * @param	$constructorArguments		array				variable arguments
	 * @return								object				instantiated class
     */
	public function makeInstance(string $class, ...$constructorArguments)
	{
		if (strtoupper(substr($class, 0, 5)) == 'TYPO3')
		{
			$object = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance($class, ...$constructorArguments);
		}
		else
		{
			$object = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance($class);
			$object->setSettings($this->settings);
			$this->callParentConstructor($object, ...$constructorArguments);
			if (method_exists($object, 'initializeObject'))
			{
				$object->initializeObject(...$constructorArguments);
			}
		}
		return $object;
	}
	
	/**
     * Read plugins on current page and add's the given flexform configuration
     *
	 * @param	-
	 * @return	void
     */
	public function getFlexformSettings()
	{
		// get flexform configuration
		$data = $this->getPi_Flexforms();
		if (array_key_exists('pi_flexform', $data))
		{
			// settings
			$ffConf = $this->getFlexform2ConfigArray($data['pi_flexform']);
			if (!empty($ffConf['settings']))
			{
				$this->settings = array_replace_recursive($this->settings, $ffConf['settings']);	
			}
			
			// other than settings
			unset($ffConf['settings']);
			if (!empty($ffConf))
			{
				$this->configuration = array_replace_recursive($this->configuration, $ffConf);
			}
		}
	}


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	/**
     * call parent constructor, if available
     *
	 * @param	$object						object				the instantiated object
	 * @param	$constructorArguments		array				variable arguments
	 * @return	void
	 */
	protected function callParentConstructor($object, ...$constructorArguments)
	{
		$parents = class_parents($object);
		$skipped = false;
		foreach ($parents as $parent)
		{
			// first parent is the "base" class, which inherits THIS constructor (of this "general trait") --> skip
			if ($skipped)
			{
				if (method_exists($parent, '__construct'))
				{
					// make reflection object
					$reflectionMethod = new \ReflectionMethod($parent, '__construct');
					$reflectionMethod->invokeArgs($object, $constructorArguments);
				}
				break;
			}
			$skipped = (!$skipped);
		}
	}

	/**
     * Select tt_content's pi_flexforms of "maagituser" elements on current page
     *
	 * @param	-
	 * @return								array				the flexform values
	 */
	protected function getPi_Flexforms()
	{
		// get flexform settings from the "maagituser" plugins of given page
		$queryBuilder = $this->makeInstance('TYPO3\\CMS\\Core\\Database\\ConnectionPool')->getQueryBuilderForTable('tt_content');	
		$queryBuilder 
			->select('tt_content.*')
			->from('tt_content')
			->where(
				$queryBuilder->expr()->eq(
					'tt_content.pid', 
					// @extensionScannerIgnoreLine
					$queryBuilder->createNamedParameter($GLOBALS['TSFE']->id, \TYPO3\CMS\Core\Database\Connection::PARAM_INT)
				),
				$queryBuilder->expr()->in(
					'tt_content.CType', 
					$queryBuilder->createNamedParameter($this->plugins, \TYPO3\CMS\Core\Database\Connection::PARAM_STR_ARRAY)
				)
		);
		$records = $queryBuilder
			->executeQuery()
			->fetchAssociative();
		return ($records!==FALSE)?$records:array();
	}


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
	/**
     * Convert given flexform value to array
     *
	 * @param	$xml						string				the pi_flexform as xml string
	 * @return								array				the flexform data as config array
     */
	private function getFlexform2ConfigArray($xml)
	{
		$flexFormTools = $this->makeInstance('TYPO3\\CMS\\Core\\Configuration\\FlexForm\\FlexFormTools');
		$flexformData = array();
		try
		{
			$xmlArray = \TYPO3\CMS\Core\Utility\GeneralUtility::xml2array($xml);
			foreach ($xmlArray as $sheets)
			{
				foreach ($sheets as $elements)
				{
					foreach ($elements as $fields)
					{
						foreach ($fields as $key => $value)
						{
							$temp = $this->getFFValue($key, $value['vDEF'], array());
							$flexformData = array_merge_recursive($flexformData, $temp);
						}
					}
				}
			}
		}
		catch (\Exception $ex) { }
		return $flexformData;
	}


	/**
     * Get the flexform value as key => value pair.
	 * Recursive function, for exploding keys with subkeys in a array structure
     *
	 * @param	$key						string				the key from pi_flexform (can be key with subkey, e.g. settings.delivery.method)
	 * @param	$value						string				the value from pi_flexform
	 * @param	$array						array				the parent array for creating recursively array structure
	 * @param	$delimiter					string				the delimiter of "keys with subkeys", default '.'
	 * @param	$key						string				the key from pi_flexform (can be key with subkey, e.g. settings.delivery.method)
	 * @return								array				the flexform data as config array
     */
	private function getFFValue($key, $value, $array, $delimiter = '.')
	{
		if (strpos($key, $delimiter) === FALSE)
		{
			$array = array($key => $value);
			return $array;
		}
		else
		{
			$segment = substr($key, 0, strpos($key, $delimiter));
			$newKey = substr($key, strpos($key, $delimiter) + 1);
			$array = array($segment => $this->getFFValue($newKey, $value, $array));
			return $array;
		}
	}
}