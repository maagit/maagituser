jQuery(document).ready(function ($) {
	// local variables
	var maagituser = $('.tx-maagituser');
	var login_button = document.getElementById('tx-maagituser_login_link');
	var logout_button = document.getElementById('tx-maagituser_logout_link');
	var login_content = $('.tx-maagituser_content');
	
	// register "click" event on login button
	if (login_button) {
		login_button.addEventListener('click', function(event) {
			event.preventDefault();
			toggleLoginLogout();
		});		
	}
	
	// register "click" event on logout button
	if (logout_button) {
		logout_button.addEventListener('click', function(event) {
			event.preventDefault();
			toggleLoginLogout();
		});
	}
	
	// register general "click" event, if login form is open
	maagituser.on('click', function(event) {
		if (event.target.className == 'tx-maagituser tx-maagituser_open') {
			// close login when clicking on bg layer
			toggleLoginLogout(true);
		}
	});
	
	// function: show/hide login/logout form
	function toggleLoginLogout(bool) {
		var loginIsOpen = (typeof bool === 'undefined') ? maagituser.hasClass('tx-maagituser_open') : bool;
		if (loginIsOpen) {
			maagituser.removeClass('tx-maagituser_open');

		} else {
			maagituser.addClass('tx-maagituser_open');
		}
	};
});