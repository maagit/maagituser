<?php
$EM_CONF[$_EXTKEY] = [
	'title' => 'MaagIT User',
	'description' => 'Versatile login and user system based on Extbase & Fluid and using latest technologies provided by TYPO3 CMS.',
	'category' => 'plugin',
	'author' => 'Urs Maag',
	'author_email' => 'info@maagit.ch',
	'author_company' => 'maagIT',
	'state' => 'stable',
	'createDirs' => '',
	'clearCacheOnLoad' => 1,
	'version' => '13.4.5',
	'constraints' => [
		'depends' => [
			'typo3' => '13.4.5-13.99.99',
			'felogin' => '13.4.5-13.99.99'
		],
		'conflicts' => [

		],
		'suggests' => [
			'maagitprovider' => '13.4.5-13.99.99'
		]
	]
];
